# File Downloader (Lab12 in OOP2015)

This is an application for downloading a URL to a file.  The purpose is to see how threads affect performance and practice using Swing's thread management classes.
Swing has some guidelines for thread usage:

* GUI updates and event listeners should run on the **Event Dispatcher Thread**.
* Long-running tasks should **not** be run on the Event Dispatcher Thread, to avoid non-responsive UIs.
* You should use a *SwingWorker* for communication between background tasks and foreground (event dispatcher) thread.

## Instructions

1. Download a ZIP copy of this repository (https://bitbucket.org/skeoop/downloader) to your workspace.  Unzip and create a new project form it.
2. Run it and try downloading a large file.  Try to use the UI during the download -- the user interface freezes.  This is because we're not using Swing threads correctly.
3. Modify the `DownloadApp` class to run the UI in the Swing Event Dispatcher thread.  In the main method, use `SwingWorker.invokeLater` for this. 
4. Rewrite the `URLReader` class to be a subclass of `SwingWorker` so you can `execute()` it from the DownloaderUI without hanging.  **Hint:** the `readUrl()` method should become (or be called from) the SwingWorker's `doInBackground()` method.
5. Modify `DownloaderUI` class. In the DownloadAction, invoke your `SwingWorker` to download the file so the UI does not hang or freeze.  
6. Add `publish` commands to the `doInBackground` method of `URLReader` so that it "publishes" an update each time 2048 bytes is read. This is so the UI can show download progress.  
7. Add code to the DownloaderUI to show progress of how many bytes have been downloaded. You can use a text field, a JProgressBar (maybe the easiest), or other solution.
8. Demonstrate that you can now download a large file without the UI freezing.
9. Document your code! You may want to reuse some of this code in future apps, so you should write good class Javadoc that explains **what** the class does, **why** (download without freezing the UI), and **how**.

## What to Submit

* Commit your work to Bitbucket as `Lab12`.

## Summary

* In interactive applications you need to insure that the UI remains responsive (no freezing!). You should use threads for any tasks that might take some time. This is especially true on mobile platforms like Android and iOS. Unlike Swing, Android will **kill** apps that become unresponsive!
* Use a *background thread* (called a *worker thread*) for tasks that some time. This is especially true for tasks involving I/O, using the network, or some significant computation.
* A *worker thread* needs to communicate progress and results with the UI thread. You need to be careful about this; in Swing, write a `SwingWorker` for for your worker thread(s).  On Android, worker threads are usually launched as `AsyncTask` objects.
* Create GUI objects and start your GUI on the Event Dispatcher thread. Use `SwingWorker.invoikeLater()` or `SwingWorker.invokeAndWait()` for this.